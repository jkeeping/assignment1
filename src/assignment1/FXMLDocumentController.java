/*
 *  {Westfield State University}
 *  {James Keeping}
 * and open the template in the editor.
 */
package assignment1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 *
 * @author jameskeeping
 */
public class FXMLDocumentController implements Initializable {
    String text2 = "";
    String text = "";
    String line = "";
    //Scanner scan = new Scanner(System.in);
    ArrayList<String> list = new ArrayList<String>();
    BufferedReader br = null;
    BufferedWriter bw = null;
    
    @FXML
    private Label label;
    
    @FXML
    private TextField inputView;
    
    @FXML
    private TextField outputView;
    
    @FXML
    private Label notice;
    
     
    
    @FXML
    private void handleInputAction (ActionEvent event) {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new ExtensionFilter("input", ".txt"));
        File sf = fc.showOpenDialog(null);
        
        
        try {      
            
             if(sf != null) {                  
            inputView.setText(sf.getAbsolutePath());          
             }      
             
             
            text = inputView.getText();
            
            br = new BufferedReader(new FileReader(text));
                 
            
           
            
        } catch (Exception e) 
        {
            System.out.println(e.getMessage()+"File not found");
            System.exit(0);
            
        }
        
        try {
            
            while( (line = br.readLine()) != null) {
                    list.add(line);       
            }
            
            }
      
        catch (Exception e) {
            System.out.println(e.getMessage() + "System error");
        }
        
        
        
       
        
    }
    
    @FXML
    private void handleOutputAction (ActionEvent event) {
        
        // System.out.println("The list has been sorted\n" + list);
         
        FileChooser fc2 = new FileChooser();
        fc2.getExtensionFilters().add(new ExtensionFilter("output", ".txt"));
        File sf2 = fc2.showOpenDialog(null);
        
        try {
             
            outputView.setText(sf2.getAbsolutePath());
          
            text2 = outputView.getText();          
            }
            
              
         catch (Exception e) {
            
            System.out.println(e.getMessage()+"File not found");
            System.exit(0);
            
        }
}
            
    @FXML
    private void handleSort (ActionEvent event) {
        //System.out.println("Sort");
        
        Collections.sort(list);
        
        boolean blank = false;
        boolean sent = false;
        
        
        
        if(text.isEmpty() || text2.isEmpty()) {
            blank = true;
            notice.setText("One or both of the fields are empty");
            
        }
        
        
        if(blank == false) {
        
        try {
             
            Writer wo = new BufferedWriter(new FileWriter(text2));
            
            for(int i = 0; i < list.size(); i++) {
                wo.write(list.get(i) + "\n");
            } 
                sent = true;
                wo.close();
                
            if(sent == true) {
                notice.setText("Your file has been sorted and sent");
                
            }    
                
        } catch (Exception e) {
            
            System.out.println(e.getMessage() + "cannot write to file");
        }

        }
    
    }
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
   
    }    
    
}
